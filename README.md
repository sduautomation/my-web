## Folder structure

### app folder | start ------------------------------

Console/
Exceptions/
Events/
	. . .
Http/
	Controllers/
		API/
			v1/
				Auth/
					. . .
				Controllers/
					. . .
				Resources/
					. . .
				Common/
					. . .
				. . .
			. . .
		Web/
			Common/
				. . .
			WebController.php
	Middleware/
		. . .
Jobs/
	. . .
Mail/
	. . .
Models/
	. . .
Notifications/
	. . .
Listeners/
	. . .
Repositories/
	. . .
Interfaces/
	. . .
Providers/
	. . .

### resources folder

js/
	assets/
		css/
			. . .
		fonts/
			. . .
		icons/
			. . .
		js/
			. . .
		. . .
	config/
		. . .  
	langs/
		. . .
	packages/
		. . .
	modules/
		common/
			components/
				ui/
					. . .
				chunks/
					. . .
				. . .
			views/
				. . .
			store/
				smodule/
					. . .
				. . .
		{modulname}/
			components/
				. . .
			configs/
				. . .
			route/
				. . .
				index.js
			store/
				smodule/
					. . .
				store.js
			views/
				. . .
			{modulename.js}
			{modulename.vue}
		. . .
	bootstrap.js
saas/
	. . .
views/
		mails/
			. . .
		pages/
			. . .
		. . .